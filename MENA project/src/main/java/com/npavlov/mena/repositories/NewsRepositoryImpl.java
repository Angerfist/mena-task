package com.npavlov.mena.repositories;

import com.npavlov.mena.models.News;
import com.npavlov.mena.repositories.contracts.NewsRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class NewsRepositoryImpl implements NewsRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public NewsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public News getNewsById(int id) {
        News news = null;
        try (Session session = sessionFactory.openSession()) {
            news = session.get(News.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (news == null) {
            throw new RuntimeException(String.format("News with id %d not found.", id));
        }
        return news;
    }

    @Override
    public void addNews(News news) {
        try (Session session = sessionFactory.openSession()) {
            session.save(news);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void editNews(News newNews) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            News news = session.get(News.class, newNews.getId());
            news.setTitle(newNews.getTitle());
            news.setDescription(newNews.getDescription());
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void deleteNews(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            News news = session.get(News.class, id);
            news.setDeleted(true);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public List<News> getAllNews() {
        String hql = "from News WHERE isDeleted < 1";
        return getNewsString(hql);
    }

    @Override
    public List<News> sortNewsByDate() {
        String hql = " from News " +
                "where isDeleted < 1" +
                "order by date desc";
        return getNewsString(hql);
    }

    @Override
    public List<News> sortNewsByTitle() {
        String hql = " from News " +
                "where isDeleted < 1" +
                "order by title";
        return getNewsString(hql);
    }

    @Override
    public List<News> filterNewsByDate(String date) {
        List<News> filtered;
        try {
            Query<News> query = sessionFactory.getCurrentSession().createQuery("from News n where date like :date and isDeleted < 1");
            query.setParameter("date", "%"+date+"%");
            filtered = query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (filtered.size() == 0) {
            throw new RuntimeException(String.format("News in %s not found", date));
        }
        return filtered;
    }

    @Override
    public List<News> filterNewsByTitle(String title) {
        List<News> filtered;
        try {
            Query<News> query = sessionFactory.getCurrentSession().createQuery("from News where title like :title and isDeleted < 1");
            query.setParameter("title", "%"+title+"%");
            filtered = query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (filtered.size() == 0) {
            throw new RuntimeException(String.format("News with title %s not found.", title.toUpperCase()));
        }
        return filtered;
    }

    private List<News> getNewsString(String hql) {
        try (Session session = sessionFactory.openSession()) {
            Query<News> query = session.createQuery(hql, News.class);
            return query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }
}
