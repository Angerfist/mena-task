package com.npavlov.mena.repositories.contracts;

import com.npavlov.mena.models.News;
import java.util.List;

public interface NewsRepository {

    News getNewsById(int id);

    void addNews(News news);

    void editNews(News newNews);

    void deleteNews(int id);

    List<News> getAllNews();

    List<News> sortNewsByDate();

    List<News> sortNewsByTitle();

    List<News> filterNewsByDate(String date);

    List<News> filterNewsByTitle(String title);


}
