package com.npavlov.mena.services;

import com.npavlov.mena.models.News;
import com.npavlov.mena.repositories.contracts.NewsRepository;
import com.npavlov.mena.services.contracts.NewsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
class NewsServicesImpl implements NewsServices {

    private NewsRepository newsRepository;

    @Autowired
    public NewsServicesImpl(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    public News getNewsById(int id) {
        return newsRepository.getNewsById(id);
    }

    @Override
    public void addNews(News news) {
        newsRepository.addNews(news);
    }

    @Override
    public void editNews(News newNews) {
        newsRepository.editNews(newNews);
    }

    @Override
    public void deleteNews(int id) {
        newsRepository.deleteNews(id);
    }

    @Override
    public List<News> getAllNews() {
        return newsRepository.getAllNews();
    }

    @Override
    public List<News> sortNewsByDate() {
        return newsRepository.sortNewsByDate();
    }

    @Override
    public List<News> sortNewsByTitle() {
        return newsRepository.sortNewsByTitle();
    }

    @Override
    public List<News> filterNewsByDate(String date) {
        return newsRepository.filterNewsByDate(date);
    }

    @Override
    public List<News> filterNewsByTitle(String title) {
        return newsRepository.filterNewsByTitle(title);
    }
}

