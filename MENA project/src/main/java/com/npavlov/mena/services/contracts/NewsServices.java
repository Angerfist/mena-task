package com.npavlov.mena.services.contracts;

import com.npavlov.mena.models.News;
import java.util.List;

public interface NewsServices {
    News getNewsById(int id);

    void addNews(News news);

    void editNews(News newNews);

    void deleteNews(int id);

    List<News> getAllNews();

    List<News> sortNewsByDate();

    List<News> sortNewsByTitle();

    List<News> filterNewsByDate(String date);

    List<News> filterNewsByTitle(String title);
}

