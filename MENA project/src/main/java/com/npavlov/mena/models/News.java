package com.npavlov.mena.models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private String date = new Date(System.currentTimeMillis()).toString();

    @Column(name = "title")
    @Size(min = 2, max = 45, message = "Title must be at least 2 characters and max 45 characters!")
    private String title;

    @Column(name = "description")
    @Size(min = 2, max = 65, message = "Description must be at least 2 characters and max 65 characters!")
    private String description;

    @Column(name = "text")
    @Size(min = 2, message = "Text must be at least 2 characters!")
    private String text;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    public News() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}

