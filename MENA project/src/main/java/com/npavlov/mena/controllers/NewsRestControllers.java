package com.npavlov.mena.controllers;

import com.npavlov.mena.models.News;
import com.npavlov.mena.services.contracts.NewsServices;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/news")
public class NewsRestControllers {

    private NewsServices newsService;

    @Autowired
    public NewsRestControllers(NewsServices newsService) {
        this.newsService = newsService;
    }

    @GetMapping("/{id}")
    public News getNewsById(@PathVariable int id) {
        try {
            return newsService.getNewsById(id);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database.");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("News with id %d not found", id)
            );
        }
    }

    @GetMapping("/all")
    public List<News> getAllNews() {
        try {
            return newsService.getAllNews();
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database.");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    "No News"
            );
        }
    }

    @GetMapping("/filter_title")
    public List<News> filterNewsByTitle(@RequestParam String title) {
        try {
            return newsService.filterNewsByTitle(title);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database.");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Not found news with title: %s", title)
            );
        }
    }

    @GetMapping("/filter_date")
    public List<News> filterNewsByDate(@RequestParam String date) {
        try {
            return newsService.filterNewsByDate(date);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database.");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Not found news since: %s", date)
            );
        }
    }

    @GetMapping("/sort_title")
    public List<News> sortNewsByTitle() {
        return newsService.sortNewsByTitle();
    }

    @GetMapping("/sort_date")
    public List<News> sortNewsByDate() {
        return newsService.sortNewsByDate();
    }

    @PostMapping("/add")
    public void addNews(@Valid @RequestBody News news) {
        newsService.addNews(news);
    }

    @PutMapping("/edit")
    public void editNews(@Valid @RequestBody News news) {
        newsService.editNews(news);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteNewsById(@PathVariable int id) {
        try {
            newsService.deleteNews(id);
        } catch (HibernateException he) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to access database.");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("Not found news with id %d", id)
            );
        }
    }

}

