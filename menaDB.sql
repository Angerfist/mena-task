CREATE DATABASE `menadb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `menadb`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(45) NOT NULL DEFAULT current_timestamp(),
  `title` varchar(45) NOT NULL,
  `description` varchar(65) NOT NULL,
  `text` longtext NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test1', 'test1', 'test1');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test2', 'test2', 'test2');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test3', 'test3', 'test3');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test4', 'test4', 'test4');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test5', 'test5', 'test5');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test6', 'test6', 'test6');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test7', 'test7', 'test7');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test8', 'test8', 'test8');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test9', 'test9', 'test9');
INSERT INTO `menadb`.`news` (`title`, `description`, `text`) VALUES ('test10', 'test10', 'test10');
