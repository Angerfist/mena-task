# MENA task

**Functionalities**<br>
----
**<details><summary>News</summary> :black_small_square: get news by id<br> :black_small_square: list all news<br> :black_small_square: filter news by title and date<br> :black_small_square: sort news by title and date<br> :black_small_square: add news<br> :black_small_square: edit news<br> :black_small_square: delete news</details>**

**About**<br>
----
1. The backend logic of the app is developed as a REST API, using the Spring MVC framework and Hibernate for relational database access.

